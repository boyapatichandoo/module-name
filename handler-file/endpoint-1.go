package handler

import "net/http"

//EndpointOne ...
func EndpointOne(w http.ResponseWriter, r *http.Request) {
	claimID := getClaimID()
	w.Write([]byte("endpoint1 " + claimID))
}

func getClaimID() string {
	return "123"
}
