package handler

import "net/http"

//EndpointTwo ...
func EndpointTwo(w http.ResponseWriter, r *http.Request) {
	claimID := getClaimID()
	w.Write([]byte("endpoint2 " + claimID))
}
