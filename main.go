package main

import (
	"net/http"

	"gitlab.com/boyapatichandoo/module-name/handler-file"
)

func main() {
	server := &http.ServeMux{}
	server.HandleFunc("/endpoint1", handler.EndpointOne)
	server.HandleFunc("/endpoint2", handler.EndpointTwo)
	http.ListenAndServe(":8000", server)
}
